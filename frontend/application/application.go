package application

import (
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend/config"
	notificationAPI "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api"
)

type Application struct {
	Cfg                 *config.Config
	notificationChannel chan notificationAPI.CloudEvent
	vwlSystem           string
}

func New(cfg *config.Config) Application {
	return Application{
		Cfg:                 cfg,
		notificationChannel: make(chan notificationAPI.CloudEvent),
		vwlSystem:           "beheer vreemdelingen v0",
	}
}
