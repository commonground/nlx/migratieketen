# Stage 1
FROM digilabpublic.azurecr.io/golang:1.21.5-alpine3.19 AS builder

# Cache dependencies
WORKDIR /build/fictief-bvv-api
COPY fictief-bvv-api/go.mod fictief-bvv-api/go.sum ./

WORKDIR /build/fictief-loket-api
COPY fictief-loket-api/go.mod fictief-loket-api/go.sum ./

WORKDIR /build/fictief-sigma-api
COPY fictief-sigma-api/go.mod fictief-sigma-api/go.sum ./

WORKDIR /build/notification-api
COPY notification-api/go.mod notification-api/go.sum ./

WORKDIR /build/frontend
COPY frontend/go.mod frontend/go.sum ./

RUN go mod download

WORKDIR /build/fictief-bvv-api
COPY fictief-bvv-api ./

WORKDIR /build/fictief-loket-api
COPY fictief-loket-api ./

WORKDIR /build/fictief-sigma-api
COPY fictief-sigma-api ./

WORKDIR /build/notification-api
COPY notification-api ./

# Copy the code
WORKDIR /build/frontend
COPY frontend/main.go .
COPY frontend/application application
COPY frontend/cmd cmd
COPY frontend/config config
COPY frontend/helpers helpers

# Build the Go files
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o server .


# Stage 2
FROM digilabpublic.azurecr.io/node:20-alpine3.19 AS node_builder

RUN corepack enable && corepack prepare pnpm@latest --activate

# Copy the static files into the container. Note: copy to a dir instead of `.`, since Parcel cannot run in the root dir, see https://github.com/parcel-bundler/parcel/issues/6578. Note: including the views dir, which is used by Parcel for tree shaking, see tailwind.config.js
WORKDIR /build/frontend

COPY frontend/package.json frontend/pnpm-lock.yaml frontend/tailwind.config.js frontend/.postcssrc frontend/.parcelrc ./

RUN pnpm install

COPY frontend/static static
COPY frontend/views views

# Build the static files
RUN pnpm run build


# Stage 3
FROM digilabpublic.azurecr.io/alpine:3.19

# Add timezones
RUN apk add --no-cache tzdata

WORKDIR /fontend

# Copy the binary from /build to the root folder of this container. Also copy the public dir that was built
COPY --from=builder /build/frontend/server .
COPY --from=node_builder /build/frontend/public ./public

# Copy the views and config dirs, which are read during runtime
COPY frontend/views views
COPY config /config

# Run the binary when starting the container
ENTRYPOINT ["./server", "serve"]

EXPOSE 8080
