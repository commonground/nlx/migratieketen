package helpers

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	openapi_types "github.com/oapi-codegen/runtime/types"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend/config"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"golang.org/x/text/number"
)

var printer = message.NewPrinter(language.Dutch)

var monthsReplacer = strings.NewReplacer( // Use a strings replacer to format the time in Dutch language. See time.shortMonthNames. IMPROVE: neater solution using a library
	"Jan", "januari",
	"Feb", "februari",
	"Mar", "maart",
	"Apr", "april",
	"May", "mei",
	"Jun", "juni",
	"Jul", "juli",
	"Aug", "augustus",
	"Sep", "september",
	"Oct", "oktober",
	"Nov", "november",
	"Dec", "december",
)

// amsLocation refers to the Dutch time zone / location
var amsLocation *time.Location

// FormattedDate is used to unmarshal date values in Dutch format directly from POST bodies
type FormattedDate openapi_types.Date

// FormattedTime is used to unmarshal datetime values in Dutch format directly from POST bodies
type FormattedTime time.Time

func init() {
	var err error
	if amsLocation, err = time.LoadLocation("Europe/Amsterdam"); err != nil {
		log.Fatalf("error getting time zone: %v", err)
	}
}

func (fd FormattedDate) MarshalText() ([]byte, error) {
	return []byte(fd.Time.In(amsLocation).Format("02-01-2006")), nil
}

func (fd *FormattedDate) UnmarshalText(b []byte) error {
	val := strings.TrimSpace(string(b)) // Get rid of possible whitespace
	if val == "" {
		return nil
	}

	// Parse the time
	t, err := time.ParseInLocation("02-01-2006", val, amsLocation)
	if err != nil {
		return err
	}

	// Set the result
	fd.Time = t
	return nil
}

func (fd FormattedDate) IsZero() bool {
	return fd.Time.IsZero()
}

func (ft FormattedTime) MarshalText() ([]byte, error) {
	return []byte(time.Time(ft).In(amsLocation).Format("02-01-2006 15:04")), nil
}

func (ft *FormattedTime) UnmarshalText(b []byte) error {
	val := strings.TrimSpace(string(b)) // Get rid of possible whitespace
	if val == "" {
		return nil
	}

	// Parse the time
	t, err := time.ParseInLocation("02-01-2006 15:04", val, amsLocation)
	if err != nil {
		return err
	}

	// Set the result
	*ft = FormattedTime(t)
	return nil
}

func (ft FormattedTime) IsZero() bool {
	return time.Time(ft).IsZero()
}

func FormatObservationValue(attribute config.Attribute, val any) string {
	// For some attribute types (e.g. checkboxes), return the formatted values instead of val
	formatter := func(v string) string {
		switch attribute.Type {
		case "text":
			if v == "" {
				return "(Onbekend)"
			}

		case "select", "radios", "checkboxes":
			for _, option := range attribute.Options {
				if option.Name == v {
					return option.Label
				}
			}

		// Format dates
		case "date":
			// Parse the input date
			t, err := time.ParseInLocation(time.DateOnly, v, amsLocation)
			if err != nil {
				log.Printf("error parsing date: %v", err)
				return ""
			}

			return FormatDate(&t)

		// Format datetimes
		case "datetime":
			// Parse the input time
			t, err := time.ParseInLocation(time.RFC3339, v, amsLocation)
			if err != nil {
				log.Printf("error parsing time: %v", err)
				return ""
			}

			return FormatTime(&t)
		}

		return v
	}

	switch v := val.(type) {
	case nil:
		return "(Onbekend)"

	case bool:
		if v {
			return "Ja"
		}

		return "Nee"

	case string:
		return formatter(v)

	// case []string:
	// 	return strings.Join(v, ", ")

	case []interface{}:
		// First convert to a slice of strings, then join
		values := make([]string, len(v))
		for i, val := range v {
			var ok bool
			vString, ok := val.(string)
			if ok {
				values[i] = formatter(vString)
			} else {
				values[i] = formatter(fmt.Sprintf("%v", val)) // Use fmt.Sprintf as fallback
			}
		}

		if len(values) == 0 {
			return "(Onbekend)"
		}

		return strings.Join(values, ", ")

	default:
		log.Printf("formatting observation with unhandled type %T (value: %v)", val, val)
	}

	return fmt.Sprintf("%v", val)
}

// FormatTime formats the specified time in the Dutch time zone and language
func FormatTime(t *time.Time) string {
	if t == nil || t.IsZero() {
		return "(Onbekend)"
	}

	return monthsReplacer.Replace(t.In(amsLocation).Format("2 Jan 2006 15:04 uur"))
}

// FormatDate formats the specified time in the Dutch time zone and language
func FormatDate(t *time.Time) string {
	if t == nil || t.IsZero() {
		return "(Onbekend)"
	}

	return monthsReplacer.Replace(t.In(amsLocation).Format("2 Jan 2006"))
}

// FormatYear returns the year of the specied time.Time instance in the Dutch time zone
func FormatYear(t *time.Time) string {
	if t == nil || t.IsZero() {
		return "(Onbekend)"
	}

	return strconv.Itoa(t.In(amsLocation).Year())
}

// NumberFormat formats the specified number as string. IMPROVE: access an interface argument (or generic) and support multiple number types
func NumberFormat(v interface{}) string {
	if v == nil {
		return ""
	}

	switch v.(type) {
	case int, int8, int16, int32, int64, uint, uint16, uint32, uint64:
		// Integer: show the number without decimals
		return printer.Sprint(v)
	default:
		// Otherwise (e.g. float): assume the number is a currency, so show the number with two fixed digits
		return printer.Sprint(number.Decimal(v, number.MinFractionDigits(2), number.MaxFractionDigits(2)))
	}

	// IMPROVE: use a library like github.com/shopspring/decimal for decimal numbers instead of float64, see https://github.com/shopspring/decimal#why-dont-you-just-use-float64
}

// Dict creates a new map from the given parameters by treating values as key-value pairs. The number of values must be even. See https://github.com/Masterminds/sprig/blob/master/dict.go and https://github.com/gohugoio/hugo/blob/master/tpl/collections/collections.go
func Dict(v ...interface{}) (map[string]interface{}, error) {
	if len(v)%2 != 0 {
		return nil, errors.New("invalid dict call, the number of arguments must be even")
	}

	dict := make(map[string]interface{}, len(v)/2)
	for i := 0; i < len(v); i += 2 {
		key, ok := v[i].(string)
		if !ok {
			return nil, errors.New("invalid dict key, must be string")
		}

		dict[key] = v[i+1]
	}

	return dict, nil
}

// ToRadioValue converts the specified input to a string
func ToRadioValue(v interface{}) string {
	// If v is a pointer to an interface, consider that interface instead
	if vv, ok := v.(*interface{}); ok {
		if vv == nil {
			return ""
		}

		v = *vv
	}

	switch val := v.(type) {
	case string:
		return val

	case nil:
		return ""

	default:
		log.Printf("unexpected input for ToRadioValue: %v (type %T)", v, v)
	}

	return ""
}

// ToCheckboxValue converts the specified input to a slice of strings
func ToCheckboxValue(v interface{}) []string {
	// If v is a pointer to an interface, consider that interface instead
	if vv, ok := v.(*interface{}); ok {
		if vv == nil {
			return nil
		}

		v = *vv
	}

	switch val := v.(type) {
	case string:
		return []string{val}

	case []string:
		return val

	case []interface{}: // IMPROVE: check if this is the only case that occurs?
		// Convert the interface slice to a string slice
		result := make([]string, len(val))
		for i, item := range val {
			var ok bool
			if result[i], ok = item.(string); !ok {
				// If the result cannot be inferred: use fmt.Sprintf as fallback (apparently never occurs, but added to be sure)
				result[i] = fmt.Sprintf("%v", item)
			}
		}

		return result

	case nil:
		return nil

	default:
		log.Printf("unexpected input for ToCheckboxValue: %v (type %T)", v, v)
	}

	return nil
}

// ToInputDate formats the specified date string, so it can be used as value for a datepicker input field
func ToInputDate(d string) string {
	// Parse the input date
	t, err := time.ParseInLocation(time.DateOnly, d, amsLocation)
	if err != nil {
		log.Printf("error parsing date: %v", err)
		return ""
	}

	// Format the parsed date
	return t.In(amsLocation).Format("02-01-2006")
}

// ToInputDateTime formats the specified datetime string, so it can be used as value for a datetimepicker input field
func ToInputDateTime(d string) string {
	// Parse the input time
	t, err := time.ParseInLocation(time.RFC3339, d, amsLocation)
	if err != nil {
		log.Printf("error parsing time: %v", err)
		return ""
	}

	// Format the parsed time
	return t.In(amsLocation).Format("02-01-2006 15:04")
}
