# available variables:
# - $vreemdeling_id - id of newly created vreemdeling. Do not delete this vreemdeling, because other tests may depend on it.

echo 'Add duplicate vreemdeling to BVV:'
resp=$(echo '{
    "data": {
        "naam": "John Doe",
        "geboortedatum": "1978-12-31"
    }
}' | curl $CURL_FLAGS \
    -H 'accept: application/json' \
    -X 'POST' \
    --data @- \
    'http://np-bvv-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen' \
)
printf '%s' "$resp" | jq .
vreemdeling_id_duplicate=$(printf '%s' "$resp" | jq --raw-output '.data.id')
if [ -z "$vreemdeling_id_duplicate" -o "$vreemdeling_id_duplicate" = 'null' ]; then
    echo "No valid vreemdeling id: $vreemdeling_id_duplicate"
    exit 1
fi

echo 'Merge vreemdelingen in BVV:'
resp=$(curl $CURL_FLAGS \
    -H 'accept: application/json' \
    -X 'POST' \
    "http://np-bvv-backend-127.0.0.1.nip.io:8080/v0/samenvoegen/$vreemdeling_id/$vreemdeling_id_duplicate" \
)
printf '%s' "$resp" | jq .

echo 'Get merged vreemdelingen in BVV:'
resp=$(curl $CURL_FLAGS \
    -H 'accept: application/json' \
    "http://np-bvv-backend-127.0.0.1.nip.io:8080/v0/samenvoegen/$vreemdeling_id" \
)
printf '%s' "$resp" | jq .
resp=$(curl $CURL_FLAGS \
    -H 'accept: application/json' \
    "http://np-bvv-backend-127.0.0.1.nip.io:8080/v0/samenvoegen/$vreemdeling_id_duplicate" \
)
printf '%s' "$resp" | jq .
