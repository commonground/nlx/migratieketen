# available variables:
# - $vreemdeling_id - id of newly created vreemdeling. Do not delete this vreemdeling, because other tests may depend on it.

echo 'Add attribute observation to SIGMA:'
resp=$(echo '{
    "data": {
        "attribute": "agressief",
        "value": true
    }
}' | curl $CURL_FLAGS \
    -H 'accept: application/json' \
    -X 'POST' \
    --data @- \
    "http://np-sigma-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen/$vreemdeling_id/observations" \
)
printf '%s' "$resp" | jq .

echo 'Get some attributes from some SIGMAs:'
resp=$(curl $CURL_FLAGS \
    -H 'accept: application/json' \
    "http://shared-loket-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen/$vreemdeling_id?attributes=agressief&sources=fictief-np" \
)
printf '%s' "$resp" | jq .
