package main

import (
	"encoding/json"
	"log/slog"
	"time"

	"github.com/nats-io/nats.go"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api"
)

type NatsClient struct {
	natsConnection      *nats.Conn
	natsSubscription    *nats.Subscription
	notificationChannel chan string
	natsChannel         chan *nats.Msg
	shutdownChannel     chan bool
	logger              *slog.Logger
	config              *Config
}

func NewNatsClient(config *Config, logger *slog.Logger) (*NatsClient, error) {
	logger.Info("connecting to nats", "addr", config.NatsAddress)

	natsConnection, err := nats.Connect(config.NatsAddress,
		nats.RetryOnFailedConnect(true),
		nats.MaxReconnects(-1),
		nats.ReconnectWait(time.Second*15),
		nats.ReconnectHandler(func(_ *nats.Conn) {
			logger.Warn("Reconnecting nats client")
		}))
	if err != nil {
		return nil, err
	}

	natsChannel := make(chan *nats.Msg)
	natsSubscription, err := natsConnection.ChanSubscribe("notifications", natsChannel)
	if err != nil {
		return nil, err
	}

	client := NatsClient{
		natsConnection:      natsConnection,
		natsSubscription:    natsSubscription,
		notificationChannel: make(chan string),
		shutdownChannel:     make(chan bool),
		natsChannel:         natsChannel,
		logger:              logger,
		config:              config,
	}

	return &client, nil
}

func (client *NatsClient) Run() error {
	go client.runClientChannel()
	return nil
}

func (client *NatsClient) PublishNotification(data []byte) error {
	msg := nats.NewMsg("notifications")
	msg.Data = data
	err := client.natsConnection.PublishMsg(msg)
	return err
}

func (client *NatsClient) runClientChannel() {
BackgroundLoop:
	for {
		select {
		case <-client.shutdownChannel:
			break BackgroundLoop
		case data := <-client.natsChannel:
			msg := string(data.Data)
			client.logger.Info("nats client channel got msg", "msg", msg)

			var model api.CloudEvent
			if err := json.Unmarshal(data.Data, &model); err != nil {
				client.logger.Error("could not parse cloud event", "msg", msg)
				continue
			}

			if !filterOnSubject(client.config.Organization.ProcesNotifications, model.Subject) {
				client.notificationChannel <- msg
			}
		}
	}

}

func (client *NatsClient) Shutdown() error {
	client.natsConnection.Close()
	client.shutdownChannel <- true
	return nil
}

// If true we want to filter the message
// If false we want to keep the message
func filterOnSubject(allowedSubjects []string, subject *string) bool {
	if len(allowedSubjects) == 0 {
		return false
	}

	if subject == nil {
		return true
	}

	for _, v := range allowedSubjects {
		if v == *subject {
			return false
		}
	}

	return true
}
