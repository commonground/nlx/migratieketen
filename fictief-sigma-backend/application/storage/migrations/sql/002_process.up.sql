BEGIN TRANSACTION;

CREATE TABLE sigma.processes (
    id UUID NOT NULL,
    process_id TEXT NOT NULL,
    vreemdeling_id TEXT NOT NULL,

    type TEXT NOT NULL,
    status TEXT NOT NULL,

    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP,

    PRIMARY KEY(id)
);

CREATE TABLE sigma.process_attributes (
    id UUID NOT NULL,
    process_id UUID NOT NULL,

    attribute TEXT NOT NULL,
    value JSON NOT NULL,

    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP,

    PRIMARY KEY(id),
    CONSTRAINT fk_process_observations_process_id FOREIGN KEY(process_id) REFERENCES sigma.processes(id)
);

COMMIT;
