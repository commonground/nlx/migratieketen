openapi: 3.0.3
info:
  title: Fictief Loket
  description: |-
    This is a fictional SIGMA Loket API that serves as an example for the "Common Ground Migratieketen Fieldlab".
    It represents a small subset of the real-world "SIGMA Loket" system, used by the various partners in the asylum system.
  contact:
    email: digilab@vng.nl
  license:
    name: EUPL 1.2
    url: https://eupl.eu/1.2/en/
  version: 0.0.0
servers:
  - url: http://shared-loket-backend-127.0.0.1.nip.io:8080/v0
  - url: https://loket.apps.digilab.network/v0
tags:
  - name: vreemdeling
    description: Everything about foreigners/aliens, or "vreemdelingen" in Dutch.
  - name: observation
    description: An attribute of something, as observed at a specific point in time by a specific actor.
  - name: proces
    description: Everthying about processes, or "proces" in Dutch.
paths:
  /vreemdelingen/{vreemdelingId}:
    get:
      tags:
        - vreemdeling
        - observation
      summary: Find observations about a vreemdeling by ID
      description: Returns the merged attributes of a vreemdeling known to various SIGMAs, and the list of observations that lead to those attribute values.
      operationId: readVreemdelingById
      parameters:
        - $ref: "#/components/parameters/pathVreemdelingId"
        - $ref: "#/components/parameters/queryAttributes"
        - $ref: "#/components/parameters/querySources"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerwerkingsActiviteit"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerwerkingsSpan"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLSysteem"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerwerker"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerantwoordelijke"
      responses:
        "200":
          $ref: "#/components/responses/VreemdelingWithObservationsResponse"
        "400":
          $ref: "#/components/responses/BadRequestErrorResponse"
        "410":
          $ref: "#/components/responses/GoneErrorResponse"
  /vreemdelingen/{vreemdelingId}/processen:
    get:
      tags:
        - vreemdeling
        - proces
      summary: Find processen about a vreemdling by ID
      description: Returns a list of all processen of a vreemdeling known to various SIGMAs.
      operationId: readProcessenByVreemdelingId
      parameters:
        - $ref: "#/components/parameters/pathVreemdelingId"
        - $ref: "#/components/parameters/queryAttributesOptional"
        - $ref: "#/components/parameters/querySources"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerwerkingsActiviteit"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerwerkingsSpan"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLSysteem"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerwerker"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerantwoordelijke"
      responses:
        "200":
          $ref: "#/components/responses/ProcessenResponse"
        "400":
          $ref: "#/components/responses/BadRequestErrorResponse"
  /processen/{procesId}:
    get:
      tags:
        - proces
      summary: Find proces by ID
      description: Returns a proces
      operationId: readProcesById
      parameters:
        - $ref: "#/components/parameters/pathProcesId"
        - $ref: "#/components/parameters/queryAttributesOptional"
        - $ref: "#/components/parameters/querySources"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerwerkingsActiviteit"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerwerkingsSpan"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLSysteem"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerwerker"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerantwoordelijke"
      responses:
        "200":
          $ref: "#/components/responses/ProcesResponse"
        "400":
          $ref: "#/components/responses/BadRequestErrorResponse"
  /processen:
    get:
      tags:
        - proces
      summary: Get all processen
      description: Returns all processen
      operationId: readProcessen
      parameters:
        - $ref: "#/components/parameters/queryAttributesOptional"
        - $ref: "#/components/parameters/querySources"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerwerkingsActiviteit"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerwerkingsSpan"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLSysteem"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerwerker"
        - $ref: "../../vwl-api/spec/openapi.yaml#/components/parameters/FSCVWLVerantwoordelijke"
      responses:
        "200":
          $ref: "#/components/responses/ProcessenResponse"
        "400":
          $ref: "#/components/responses/BadRequestErrorResponse"

components:
  schemas:
    # attributes (camelCase):
    vreemdelingId:
      description: V-number of vreemdeling.
      type: string
      pattern: "^[0-9]{10}$"
      example: "1234567890" # copied from example images at https://ind.nl/nl/vreemdelingendocumenten/verblijfsdocument-model-2020
    observationId:
      description: UUID of observation.
      type: string
      format: uuid
      example: "00e6eaf5-e479-462d-8ed2-460cf6da107f"
    procesId:
      description: UUID of proces.
      type: string
      format: uuid
      example: "00e6eaf5-e479-462d-8ed2-460cf6da107f"
    # entities (PascalCase)
    Error:
      description: The error that occured while processing this request.
      type: object
      properties:
        message:
          type: string
          example: foo went wrong
      required:
        - message
      example:
        message: foo went wrong
    Observation:
      description: Moment of registration of a specific observation of a vreemdeling.
      type: object
      properties:
        id:
          $ref: "#/components/schemas/observationId"
        attribute:
          type: string
          example: "spreektalen"
        value:
          example: ["nl", "en"]
        source:
          type: string
          example: "fictief-coa"
        createdAt:
          type: string
          format: date-time
          example: "2023-12-12T15:00:00Z"
        invalidatedAt:
          type: string
          format: date-time
          example: "2023-12-12T15:00:00Z"
      required:
        - id
        - attribute
        - value
        - source
        - createdAt
      example:
        id: 00e6eaf5-e479-462d-8ed2-460cf6da107f
        attribute: spreektalen
        value:
          - nl
          - en
        source: fictief-coa
        createdAt: "2023-12-12T15:04:05Z"
    Attribute:
      type: object
      description: merged value of an attribute, including all observations about it.
      properties:
        value:
          example: John Snow
        observations:
          type: array
          items:
            $ref: "#/components/schemas/Observation"
          example:
            - attribute: alias
              createdAt: "2023-12-13T17:55:41Z"
              id: 00000000-0000-0000-0000-000000000000
              source: fictief-coa
              value: John Snow
      required:
        - value
        - observations
      example:
        value: John Snow
        observations:
          - attribute: alias
            createdAt: "2016-01-02T15:04:05Z"
            id: 00000000-0000-0000-0000-000000000000
            source: fictief-coa
            value: John Snow
    VreemdelingWithObservations:
      description: A known vreemdeling, with merged attributes and full list of relevant observations.
      type: object
      properties:
        id:
          $ref: "#/components/schemas/vreemdelingId"
      additionalProperties:
        $ref: "#/components/schemas/Attribute"
      required:
        - id
      example:
        id: "1234567890"
        alias:
          value: John Snow
          observations:
            - id: 00000000-0000-0000-0000-000000000001
              attribute: alias
              createdAt: "2023-12-13T17:55:41Z"
              source: fictief-coa
              value: John Snow
        spreektalen:
          value:
            - nl
            - en
          observations:
            - id: 00000000-0000-0000-0000-000000000002
              attribute: alias
              createdAt: "2023-12-13T17:55:41Z"
              source: fictief-coa
              value: ["nl", "en"]
    Proces:
      description: Proces with its attributes
      type: object
      properties:
        id:
          type: string
          format: uuid
          example: 00000000-0000-0000-0000-000000000002
        procesId:
          type: string
          pattern: "^[0-9]{10}$"
          example: "1234567890"
        vreemdelingId:
          type: string
          pattern: "^[0-9]{10}$"
          example: "1234567890"
        type:
          type: string
          example: "inbewaringstelling"
        source:
          type: string
          example: "fictief-coa"
        status:
          type: string
          example: "opgestart"
        createdAt:
          type: string
          format: date-time
          example: "2023-12-13T17:55:41Z"
        deletedAt:
          type: string
          format: date-time
          example: "2023-12-13T17:55:41Z"
        attributes:
          type: object
          additionalProperties:
            $ref: "#/components/schemas/Attribute"
      required:
        - id
        - procesId
        - vreemdelingId
        - type
        - source
        - status
        - createdAt
      example:
        id: 00e6eaf5-e479-462d-8ed2-460cf6da107f
        procesId: "1234567890"
        vreemdelingId: "1234567890"
        type: "inbewaringstelling"
        source: "fictief coa"
        status: "opgestart"
        createdAt: "2023-12-19T15:00:00Z"
        attributes:
          spreektalen:
            value: ["nl", "en"]
            observations:
              - id: 00e6eaf5-e479-462d-8ed2-460cf6da107f
                attribute: spreektalen
                value: ["nl", "en"]
                source: "fictief-coa"
                createdAt: "2023-12-19T15:00:00Z"
                invalidatedAt: "2023-12-19T15:00:00Z"
    VreemdelingWithObservationsResponse:
      description: Vreemdeling with observations response
      type: object
      properties:
        data:
          $ref: "#/components/schemas/VreemdelingWithObservations"
      required:
        - data
      example:
        data:
          id: "1234567890"
          alias:
            value: John Snow
            observations:
            - id: 00000000-0000-0000-0000-000000000001
              attribute: alias
              created_at: "2023-12-13T17:55:41Z"
              source: fictief-coa
              value: John Snow
          spreektalen:
            value: ["nl", "en"]
            observations:
            - id: 00000000-0000-0000-0000-000000000002
              attribute: alias
              created_at: "2023-12-13T17:55:41Z"
              source: fictief-coa
              value: ["nl", "en"]
    ProcesResponse:
      description: Proces response
      type: object
      properties:
        data:
          $ref: "#/components/schemas/Proces"
      required:
      - data
      example:
        data:
          id: 00e6eaf5-e479-462d-8ed2-460cf6da107f
          procesId: "1234567890"
          vreemdelingId: "1234567890"
          type: "inbewaringstelling"
          status: "opgestart"
          source: "fictief-coa"
          createdAt: "2023-12-19T15:00:00Z"
          attributes:
            spreektalen:
              value: ["nl", "en"]
              observations:
              - id: 00e6eaf5-e479-462d-8ed2-460cf6da107f
                attribute: spreektalen
                value: ["nl", "en"]
                source: "fictief-coa"
                createdAt: "2023-12-19T15:00:00Z"
                invalidatedAt: "2023-12-19T15:00:00Z"
    ProcessenResponse:
      description: Processen response
      type: object
      properties:
        data:
          type: array
          items:
            $ref: "#/components/schemas/Proces"
          example:
          - id: 00e6eaf5-e479-462d-8ed2-460cf6da107f
            procesId: "1234567890"
            vreemdelingId: "1234567890"
            type: "inbewaringstelling"
            status: "opgestart"
            source: "fictief-coa"
            createdAt: "2023-12-19T15:00:00Z"
            attributes:
              spreektalen:
                value: ["nl", "en"]
                observations:
                - id: 00e6eaf5-e479-462d-8ed2-460cf6da107f
                  attribute: spreektalen
                  value: ["nl", "en"]
                  source: "fictief-coa"
                  createdAt: "2023-12-19T15:00:00Z"
                  invalidatedAt: "2023-12-19T15:00:00Z"
      required:
      - data
      example:
        data:
        - id: 00e6eaf5-e479-462d-8ed2-460cf6da107f
          procesId: "1234567890"
          vreemdelingId: "1234567890"
          type: "inbewaringstelling"
          status: "opgestart"
          source: "fictief-coa"
          createdAt: "2023-12-19T15:00:00Z"
          attributes:
            spreektalen:
              value: ["nl", "en"]
              observations:
              - id: 00e6eaf5-e479-462d-8ed2-460cf6da107f
                attribute: spreektalen
                value: ["nl", "en"]
                source: "fictief-coa"
                createdAt: "2023-12-19T15:00:00Z"
                invalidatedAt: "2023-12-19T15:00:00Z"
  parameters:
    pathVreemdelingId:
      name: vreemdelingId
      in: path
      description: ID of vreemdeling to return.
      required: true
      schema:
        $ref: "#/components/schemas/vreemdelingId"
      example: "1234567890" # copied from example images at https://ind.nl/nl/vreemdelingendocumenten/verblijfsdocument-model-2020
    pathProcesId:
      name: procesId
      in: path
      description: ID of proces to return.
      required: true
      schema:
        $ref: "#/components/schemas/procesId"
      example: 00e6eaf5-e479-462d-8ed2-460cf6da107f
    queryAttributes:
      name: attributes
      in: query
      description: Comma-separated list of required attributes to show.
      required: true
      style: form
      explode: false
      schema:
        type: array
        items:
          type: string
          pattern: "^[a-z]+$"
          example: spreektalen
        example:
          - spreektalen
      example:
        - alias
        - spreektalen
    queryAttributesOptional:
      name: attributes
      in: query
      description: Comma-separated list of attributes to show.
      style: form
      explode: false
      schema:
        type: array
        items:
          type: string
          pattern: "^[a-z]+$"
          example: spreektalen
        example:
          - spreektalen
      example:
        - alias
        - spreektalen
    querySources:
      name: sources
      in: query
      description: Comma-separated list of SIGMA sources to query. Query all sources if not specified.
      required: false
      style: form
      explode: false
      schema:
        type: array
        items:
          type: string
          pattern: "^[a-z]+(-[a-z]+)*$"
          example: fictief-coa
        example:
          - fictief-coa
      example:
        - fictief-coa
        - fictief-ind
  requestBodies: {}
  responses:
    BadRequestErrorResponse:
      description: Invalid input supplied.
      content:
        application/json:
          schema:
            type: object
            properties:
              errors:
                type: array
                items:
                  $ref: "#/components/schemas/Error"
                example:
                  - message: foo went wrong
            required:
              - errors
            example:
              errors:
                - message: foo went wrong
    GoneErrorResponse:
      description: ID not found (but the route itself is valid).
      content:
        application/json:
          schema:
            type: object
            properties:
              errors:
                type: array
                items:
                  $ref: "#/components/schemas/Error"
                example:
                  - message: foo not found
            required:
              - errors
            example:
              errors:
                - message: foo not found
    VreemdelingWithObservationsResponse:
      description: Successful operation
      headers:
        FSC-VWL-Verwerkings-Activiteit:
          $ref: "../../vwl-api/spec/openapi.yaml#/components/headers/FSCVWLVerwerkingsActiviteit"
        FSC-VWL-Verwerkings-Span:
          $ref: "../../vwl-api/spec/openapi.yaml#/components/headers/FSCVWLVerwerkingsSpan"
        FSC-VWL-Systeem:
          $ref: "../../vwl-api/spec/openapi.yaml#/components/headers/FSCVWLSysteem"
        FSC-VWL-Verantwoordelijke:
          $ref: "../../vwl-api/spec/openapi.yaml#/components/headers/FSCVWLVerantwoordelijke"
        FSC-VWL-Verwerker:
          $ref: "../../vwl-api/spec/openapi.yaml#/components/headers/FSCVWLVerwerker"
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/VreemdelingWithObservationsResponse"
    ProcesResponse:
      description: proces response
      headers:
        FSC-VWL-Verwerkings-Activiteit:
          $ref: "../../vwl-api/spec/openapi.yaml#/components/headers/FSCVWLVerwerkingsActiviteit"
        FSC-VWL-Verwerkings-Span:
          $ref: "../../vwl-api/spec/openapi.yaml#/components/headers/FSCVWLVerwerkingsSpan"
        FSC-VWL-Systeem:
          $ref: "../../vwl-api/spec/openapi.yaml#/components/headers/FSCVWLSysteem"
        FSC-VWL-Verantwoordelijke:
          $ref: "../../vwl-api/spec/openapi.yaml#/components/headers/FSCVWLVerantwoordelijke"
        FSC-VWL-Verwerker:
          $ref: "../../vwl-api/spec/openapi.yaml#/components/headers/FSCVWLVerwerker"
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/ProcesResponse"
    ProcessenResponse:
      description: List of processen
      headers:
        FSC-VWL-Verwerkings-Activiteit:
          $ref: "../../vwl-api/spec/openapi.yaml#/components/headers/FSCVWLVerwerkingsActiviteit"
        FSC-VWL-Verwerkings-Span:
          $ref: "../../vwl-api/spec/openapi.yaml#/components/headers/FSCVWLVerwerkingsSpan"
        FSC-VWL-Systeem:
          $ref: "../../vwl-api/spec/openapi.yaml#/components/headers/FSCVWLSysteem"
        FSC-VWL-Verantwoordelijke:
          $ref: "../../vwl-api/spec/openapi.yaml#/components/headers/FSCVWLVerantwoordelijke"
        FSC-VWL-Verwerker:
          $ref: "../../vwl-api/spec/openapi.yaml#/components/headers/FSCVWLVerwerker"
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/ProcessenResponse"
